<?php defined('SYSPATH') or die('No direct script access.');

return array(
    'email_from' => array('turisticgis@gis3w.it'=>'TuristicGis'),
    'host_main' => 'http://turisticgis.gis3w.it',
    'html_tag' => array(
        'title' => 'TuristicGis::Version'.SAFE::VERSION,
    ),
);

<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>' ?>
<stylesheet>
    <any>
            <td border.color="gray" border.type="left+top+right" padding="5px"></td>
            <table border.color="gray" border.type="bottom"></table>
    </any>
    <td>
        <h3 margin="0px 0px 0px 0px"></h3>
    </td>
    <td class="title-section-1" background.color="#87CEFA"></td>
    <td class="title-section-2" background.color="#FFFF66"></td>
    <td class="title-section-3" background.color="#66FF99"></td>
    <td class="checklist-headers" background.color="#d2d2d2"></td>
    <td class="checklist-question" width="400px"></td>
    <td class="checklist-result" width="100px"></td>
    <h3 class="section-title" text-align="center"></h3>
    <dynamic-page margin="70px 40px 40px 40px "></dynamic-page>
    <any class="red" color="#FA603D"></any>
    <any class="bold" font-style="bold"></any>
    
</stylesheet>

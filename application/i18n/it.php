<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
    'Home' => 'Home',
    'Administration' => 'Amministrazione',
    'Users' => 'Utenti',
    'Logout' => 'Esci',
    'DENIED ACCESS' => 'ACCESSO NEGATO',
    'Denied access by Auth_STRICT: not authentication' => 'Accesso negato da Auth_STRICT: no autenticato',
    'Attention \'tb\' parm is not present in the request' => 'Attenzione il parametro \'tb\' non è presente nella richiesta',
	'list_capability_denied' => 'Non si hanno sufficienti permessi di visualizzazione. Tutti i dati, o parte di essi, potrebbero non essere mostrati',
    'Logins' => 'Accessi',
    'Last login' => 'Ultimo accesso',
    'Street' => 'Via',
    'Number' => 'Numero',
    'City' => 'Città',
    'Documents' => 'Documenti',
    'add' => 'aggiungi',
    'search' => 'ricerca',
    'Year' => 'Anno',
    'reinsert_question' => 'Si desidera inserire una nuova scadenza?',
    'empty_table' => 'Tabella senza record',
    'rows' => 'righe',
    'find' => 'ricerca',
    'view' => 'mostra',
    'from' => 'da',
    'to' =>'a',
    'of' => 'di',
    'previous' => 'precedente',
    'next' => 'successivo',
    'Confirm' => 'Conferma',
    'save' => 'Salva',
    'remove' => 'Rimuovi',
    'cancel' => 'Annulla',
    'send' => 'Invia',
    'save_confirm' => 'Si dichiara di aver controllato la correttezza dei dati inseriti?',
    'Name' => 'Nome',
    'Surname' => 'Cognome',
    'Description' => 'Descrizione',
    'description' => 'descrizione',
	'Data sheet' => 'Scheda',
	'Itinerary' => 'Itinerario',
    'Itineraries' => 'Itinerari',
    'Published' => 'Pubblicato',
	'Information' => 'Informazione',
	'Exit from itinerary' => 'Esci dall\'itinerario',
	'You are currently viewing the elements of the following itinerary' => 'Stai visualizzando gli elementi del seguente itinerario',
	'To view all the elements again, exit from itinerary' => 'Per visualizzare nuovamente tutti gli elementi esci dall\'itinerario',
	'Info section' => 'Informazioni',
	'poi' => 'Punto di interesse',
	'path' => 'Percorso',
	'itinerary' => 'Itinerario',
	'area' => 'Area',
	'Poi section' => 'Punti di interesse',
	'Path section' => 'Percorsi',
	'Itinerary section' => 'Itinerari',
	'Area section' => 'Aree',
	'Everytype section' => 'Luoghi da visitare',
	'Period_schedule' => 'Periodi e orari',
	'transportation_types' => 'Percorribilità',
	'features' => 'Caratteristiche',
	'Category' => 'Categoria',
	'categories' => 'Categorie',
	'measures' => 'Misure',
	'Urls' => 'Approfondimenti',
	'Type' => 'Tipo',
	'Teaser' => 'Riferimento',
	'search' => 'Cerca',
	'Search' => 'Cerca',
	'Search Panel' => 'Pannello di ricerca',
	'What are you looking for?' => 'Cosa stai cercando?',
	'page_not_found' => 'Pagina non trovata',
	'not_found' => 'Non trovato',
	'Elements' => 'Luoghi da visitare',
	'Zoom to default extent' => 'Ritorna all\'estensione iniziale della mappa',
 
    'Close' => 'Chiudi',    
    'close' => 'Chiudi',
    'Tasks' => 'Mansioni',
   
    'first' => 'primo',
    'last' => 'ultimo',
	'View data sheet' => 'Vedi scheda',
    'click_to_select' => 'Click con il mouse per selezionare',
   
    'Province' => 'Provincia',
    'Address' => 'Indirizzo',
   
    
    'apply' => 'Invia',
    'select' => 'Seleziona',
    
    'Color' => 'Colore',
    'Width' => 'Larghezza',
    
    'Pages' => 'Pagine',
    'Alphanumeric ID' => 'Identificativo alfanumerico',
    'Body' => 'Corpo pagina',
    
    
    
    //TRADUZIONI DELLE VALIDAZIONI
    ':field must not be empty' => ':field non deve essere vuoto',
    ':field does not match the required format' => ':field non ha il formato giusto (gg/mm/aaaa)',
    ':field does not match the required format (hh:mm)' => ':field non ha il formato giusto (hh::mm)',
    'download_failure' => 'Impossibile scaricare il file',
    'Insert date' => 'Data inserimento',
    'Update date' => 'Ultima modifica',
    'remove_confirm' => 'Sei sicuro che vuoi eliminare il record dal database?',
    'operation_success' => 'L\'operazione è andata a buon fine!',
    'yes' => 'si',
    'Yes' => 'Si',
    'Not applicable' => 'Non applicabile',
    'warning' => 'Attenzione',
    'error' => 'Errore',
    'success' => 'Successo',
    'select_file' => 'Seleziona un file',
    
    'Max scale' => 'Scala massima',
   
    'Urls poi' => 'Collegameti',
    'Urls path' => 'Collegameti',
    'Urls area' => 'Collegameti',
    'Urls itineraries' => 'Collegameti',
    'Description url' => 'Descrizione',

    'Document' => 'Documento',
  
    
    'Birth date' => 'Data di nascita',
    'Birth place' => 'Luogo di nascita',
  
    'Roles' => 'Ruoli',
    
    'Areas' => 'Aree',
    'Areas of interest' => 'Aree di interesse',
    
    'Title' => 'Titolo',
    'Length' => 'Lunghezza',
    'length' => 'Lunghezza',
    'Altitude gap' => 'Dislivello',
    'altitude_gap' => 'Dislivello',
    'General features' => 'Caratteristiche generali',
    'Information url' => 'URL info',
    'Typologies' => 'Tipologie',
    'typologies' => 'tipologie',
    'Geodata' => 'Dato spaziale',
    'Accessibility' => 'Accessibilità',
    'Reasons' => 'Motivi di interesse',
	'Reason' => 'Motivi di interesse',
	'Plus_information' => 'Informazioni aggiuntive',
	'Inquiry' => 'Contatti',
    'Main typology' => 'Tipologia principale',
    'typology_id' => 'Tipologia principale',
    'Period schedule' => 'Periodi e orari',
    'general_informations' => 'Informazioni generali', 
    'More informations' => 'Motivi di interesse',
    'Request informations' => 'Richiesta informazioni',
    
    'actions' => 'Azioni',
    
    'Image' => 'Immagine',
    
    'Points of interest' => 'Punti di interesse',
	'Pois' => 'Punti di interesse',
    'Paths' => 'Percorsi',
	'Areas' => 'Aree',
    
    'Images to upload' => 'Immagini da caricare',
    'Videos to embed' => 'Video da incorporare',
   
    'global-typology-data' => 'Tipologia',
    'global-page-data' => 'Pagina da inserire/modificare',
    
    'capabilitties-data' => 'Capability',
    'foreign-capabilities-data' => 'Ruoli',
    'roles-data' => 'Ruolo',
    
    'user-data-data' => 'Dati',
    'user-data-login' => 'Accesso',
    'user-data-contact' => 'Contatti',
    'user-foreign-data' => 'Collegamenti',
    
    'itinerary-data' => 'Dati generali dell\'itinerario',
    'itinerary-foreign-data' => 'Percorsi e punti di interesse associati',
    'Itinerary-block-data' => 'Altri dati accessori',
    
    'path-data' => 'Dati generali del percorso',
    'path-foreign-data' =>'Dati accessori',
    'path-block-data' => 'Altri dati accessori',
    
    'poi-data' => 'Dati generali del punto',
    'poi-foreign-data' =>'Dati accessori',
    'poi-block-data' => 'Altri dati accessori',
    
    'area-data' => 'Dati generali dell\'area',
    'area-foreign-data' =>'Collegamenti',
    'area-block-data' => 'Altri dati accessori',
    
    
    
    
    'for'=> 'per',
  
    // MSG
    'Do you check data?' => 'Si dichiara di aver controllato la correttezza dei dati inseriti?',
    'Are you sure to delete record?' =>'Sei sicuro che vuoi eliminare il record dal database?',
    'To continue is necessary to save' => 'Per proseguire è necessario effettuare il salvataggio',
    'Do you want insert a new record?' =>'Si desidera inserire un nuovo record?',
    
    'typology_icon_marker_requested' => 'Per proseguire è necessario definire le icone ed i marker per le varie tipologie',
	'not_configured' => 'non configurato',
    
);